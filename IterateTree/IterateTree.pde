/*
 IterateTree Demo
 by James Swineson, 2015-11-02
 For demonstration purpose only.
 */

float random_range = 0.035; // unit: radian
float iterate_range = 1;    // unit: px 
float iterate_range_max = 15;
float stop_render_percentage = 0.01;
int node_num = 500;
color color_start = color(240, 30, 100);
color color_end = color(52, 255, 141);
int alpha_min = 15;
int alpha_max = 180;
int framerate = 15;

int iterate_count;
Dot[] dots;

color blend(color a, color b, float percent_of_a) {
  return color(percent_of_a * red(a) + (1 - percent_of_a) * red(b), 
    percent_of_a * green(a) + (1 - percent_of_a) * green(b), 
    percent_of_a * blue(a) + (1 - percent_of_a) * blue(b));
}

class Dot {
  float x, y, last_x, last_y;
  int alpha;
  float direction;

  Dot(float x, float y, float direction, int alpha) {
    this.x = x;
    this.y = y;
    this.last_x = x;
    this.last_y = y;
    this.direction = direction;
    this.alpha = alpha;
  }

  void iterate() {
    this.last_x = this.x;
    this.last_y = this.y;
    this.direction = direction + random(-random_range, random_range);
    this.x = this.x + random(iterate_range) * cos(this.direction);
    this.y = this.y + random(iterate_range) * sin(this.direction);
  }

  void display(color c) {
    //set((int)this.x, (int)this.y, c);
    stroke(c, this.alpha);
    line(this.x, this.y, this.last_x, this.last_y);
  }
}

void setup()
{
  size(600, 600);
  smooth();
  frameRate(framerate);
  dots = new Dot[node_num];
  for (int i = 0; i < node_num; i++) {
    dots[i] = new Dot(width / 2, height / 2, random(0, TWO_PI), (int)random(alpha_min, alpha_max));
  }
  background(color(255, 255, 255));
}

void draw()
{
  for (Dot d : dots) {
    for (int i = (int)random(iterate_range, iterate_range_max / iterate_range); i > 0; i--) {
      d.iterate();
      if (!(d.x > width || d.x < 0 || d.y > height || d.y < 0)) d.display(blend(color_end, color_start, (abs(d.x - width / 2) / width * 2)));
    }
    if ((d.x > width || d.x < 0 || d.y > height || d.y < 0) && random(1) < stop_render_percentage / (node_num / iterate_range_max * 2) ) {
      noLoop();
      //setup();
    }
  }
}