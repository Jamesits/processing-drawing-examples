final float dot_range[] = {1, 5, 7, 9, 11, 16, 18, 23, 27, 37};
final float dot_size = 3;

PVector actual_point_pos(float percent, float startX, float startY, float endX, float endY) {
  //float percent = len / sqrt(pow(abs(endX - startX), 2) + pow(abs(endY - startY), 2));
  PVector p = new PVector();
  p.x = startX + (endX - startX) * percent;
  p.y = startY + (endY - startY) * percent;
  print(percent, ", (", p.x, ", ", p.y, ")\n");
  return p;
}

void draw_light(float startX, float startY, float endX, float endY) {
  for (float i: dot_range) {
    PVector p = actual_point_pos(i / dot_range[dot_range.length - 1], startX, startY, endX, endY);
    ellipse(p.x, p.y, dot_size, dot_size);
  }
}

void setup() {
  size(600, 600);
  smooth();
  background(color(0, 0, 0));
  fill(color(255, 255, 255));
  draw_light(300, 300, 0, 0);
}

void draw() {
  
}