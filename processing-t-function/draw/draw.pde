// Settings
float left_heart_centerX = 430;
float left_heart_centerY = 135;
float right_heart_centerX = 537;
float right_heart_centerY = 135;
float speed = 3;
float size_min = 20;
float size_max = 60;
float framerate = 30;

PImage android, heart;

void draw_function_t(PImage im, float centerX, float centerY, float currentFrame, float totalFrame, float speed, float size_min, float size_max) {
  float pWidth = abs(sin(speed * currentFrame / totalFrame)) * (size_max - size_min) + size_min;
  float pHeight = pWidth / im.width * im.height;
  float posX = centerX - pWidth / 2;
  float posY = centerY - pHeight / 2;
  image(im, posX, posY, pWidth, pHeight);
}

void setup() {
  size(960, 540);
  surface.setResizable(true);
  frameRate(framerate);
  android = loadImage("android.jpg");
  heart = loadImage("heart.png");
}

void draw() {
  image(android, 0, 0);
  draw_function_t(heart, left_heart_centerX, left_heart_centerY, frameCount % (2*frameRate), frameRate, speed, size_min, size_max);
  draw_function_t(heart, right_heart_centerX, right_heart_centerY, frameCount % (2*frameRate), frameRate, speed, size_min, size_max);
}