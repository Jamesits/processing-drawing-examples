/*
 Libraries needed: 
   Video: https://processing.org/reference/libraries/video/index.html
   OpenCV-Processing: https://github.com/atduskgreg/opencv-processing
   
 put all libraries under ~/Documents/Processing/libraries
*/

boolean show_debug_information = true;
boolean show_egg = true;
int camera_width = 1280;
int camera_height = 720;
int calibration_finished_contour_max = 150;
int environment_changed_detection_contour_min = 1300;
long splash_screen_delay = 5000;
int history = 40;
int nMixtures = 20;
double default_backgroundRatio = 0.5;
double calibrating_backgroundRatio = 0.9;

import gab.opencv.*;
import processing.video.*;
import java.awt.*;

Capture video;
OpenCV opencv;
boolean first_start = true;
boolean hasCalibrated = false;
double backgroundRatio = default_backgroundRatio;
long start_time = 0;
int contour_count = 0;

void setup() {
  size(1280, 720);
  draw_hints("Starting camera...");
  video = new Capture(this, camera_width, camera_height);
  opencv = new OpenCV(this, camera_width, camera_height);
  if (show_egg) opencv.loadCascade(OpenCV.CASCADE_FRONTALFACE);
  opencv.startBackgroundSubtraction(history, nMixtures, backgroundRatio);
  video.start();
  start_time = millis();
}

void captureEvent(Capture c) {
  c.read();
}

void draw_hints(String text) {
  fill(0, 180);
  rect(0, 0, width, height);
  fill(255, 255, 255);
  textAlign(CENTER, CENTER);
  textSize(48);
  text(text, 0, 0, width, height);
}

void print_debug_information() {
  if (contour_count == 0) contour_count = opencv.findContours().size();
  String s = "LiveCamWalkDetector Debug Information\n\n"
    + "Current status: " + (first_start ? "Performing First-time Calibration" : (hasCalibrated ? "Normal" : "In Calibration") )+ "\n"
    + "===== Processing Runtime =====\n"
    + "Initializaion time: " + start_time + "ms\n"
    + "Uptime:" + (millis() - start_time) + "ms\n"
    + "Window resolution: " + width + "*" + height + "\n"
    + "Video resolution: " + camera_width + "*" + camera_height + "\n"
    + "Total frames: " + frameCount + "\n"
    + "Current framerate: " + frameRate + "fps\n"
    + "Average framerate: " + (float)frameCount * 1000 / (millis() - start_time) + "fps\n"
    + "===== OpenCV Settings =====\n"
    + "Acceptable contours range: " + calibration_finished_contour_max + " - " + environment_changed_detection_contour_min + "\n"
    + "Contours detected: " + contour_count + "\n"
    + "Default background ratio: " + default_backgroundRatio + "\n"
    + "Background ratio for calibration: " + calibrating_backgroundRatio + "\n"
    + "Current background ratio: " + backgroundRatio + "\n"
    + "History frames: " + history + "\n"
    + "Mixtures: " + nMixtures + "\n"
  ;
  fill(255, 0, 0);
  textSize(14);
  textAlign(LEFT, LEFT);
  text(s, 5, 20);
}

void draw() {
  opencv.loadImage(video);
  image(video, 0, 0);
  opencv.updateBackground();
  opencv.dilate();
  opencv.erode();
  contour_count = 0;

  if (first_start) {
    boolean has_detected_face = false;
    if (show_egg) {
      Rectangle[] faces = opencv.detect();
      for (Rectangle face : faces) {
        if (face.height > height / 3 || face.width > width / 5) {
          has_detected_face = true;
          break;
        }
      }
    } 
    if (has_detected_face) {
      draw_hints("Hey, you are beautiful!\nbut it's a little...wired to be looked at.\nPlease, please don't stare at me! >_<");
    } else {
      draw_hints("Performing first-time background calibration...\nPlease keep camera steady\n and make sure nothing is in camera's field of view.");
    }
    if ( millis() - start_time >= splash_screen_delay && opencv.findContours().size() < calibration_finished_contour_max && !has_detected_face) {
      first_start = false;
      hasCalibrated = true;
    }
  } else {
    contour_count = opencv.findContours().size();
    if (contour_count > environment_changed_detection_contour_min) {
      hasCalibrated = false;
      backgroundRatio = calibrating_backgroundRatio;
    } else if (contour_count < calibration_finished_contour_max) {
      hasCalibrated = true;
      backgroundRatio = default_backgroundRatio;
    }

    if (!hasCalibrated) {
      draw_hints("Performing background calibration...\nPlease keep camera steady\n and don't enter the field of view.");
    } else {
      noFill();
      stroke(0, 0, 0);
      strokeWeight(3);
      for (Contour contour : opencv.findContours()) {
        contour.draw();
      }
    }
  }
  if (show_debug_information) print_debug_information();
}