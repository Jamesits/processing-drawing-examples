// by James Swineson, 2015-11-10

int num = 15;
int dotMax = 10;
int dotMin = 2;
int framerate = 30;
float speed = 0.3;
float cycleAngle = 0.2;

Compound[] c;

int dotSizeByFrame(int size) {
  return abs(((int)(frameCount * speed) + size) % (2 * dotMax / dotMin) - dotMax / dotMin) + dotMin;
}

class Compound {
  float centerX, centerY;
  float direction;
  Compound(float x, float y, float direction) {
    this.centerX = x;
    this.centerY = y;
    this.direction = direction;
  }
  void draw(){
    // part one
    Pen p0 = new Pen(centerX, centerY).putDown()
      .pdMove(15, direction - 156)
      .dot(dotSizeByFrame(3))
      .pdMove(5)
      .dot(dotSizeByFrame(5))
      .pdMove(100, direction - 95)
      .dot(dotSizeByFrame(5));
    Pen p1 = new Pen(p0).putDown()
      .pdMove(180, direction - 135)
      .dot(dotSizeByFrame(5));
    Pen p2 = new Pen(p0).putDown()
      .pdMove(85, direction - 30)
      .dot(dotSizeByFrame(7));
    Pen p3 = new Pen(p2).putDown();
    p2.pdMove(65, direction - 45)
      .dot(dotSizeByFrame(5));
    Pen p4 = new Pen(p2).putDown();
    p2.moveTo(p1);
    p1.moveTo(p3)
      .pdMove(80)
      .dot(dotSizeByFrame(5));
    p1.moveTo(p4);
    
    // part two
    Pen p10 = new Pen(centerX, centerY).putDown()
      .pdMove(15, direction - 140)
      .dot(dotSizeByFrame(3))
      .pdMove(5)
      .dot(dotSizeByFrame(5))
      .pdMove(120, direction - 95)
      .dot(dotSizeByFrame(5));
    Pen p11 = new Pen(p10).putDown()
      .pdMove(100, direction + 30)
      .dot(dotSizeByFrame(5));
    Pen p12 = new Pen(p10).putDown()
      .pdMove(70, direction + 135)
      .pdMove(200, direction - 150)
      .dot(dotSizeByFrame(7));
    Pen p13 = new Pen(p10).putDown()
      .pdMove(80, direction - 60)
      .dot(dotSizeByFrame(3));
    new Pen(p11).putDown().moveTo(p13);
    new Pen(p12).putDown().moveTo(p10);
    new Pen(p12).putDown().moveTo(p13);
    
    // part 3
    Pen p20 = new Pen(centerX, centerY).putDown()
      .pdMove(15, direction - 140)
      .dot(dotSizeByFrame(3))
      .pdMove(5)
      .dot(dotSizeByFrame(5))
      .pdMove(150, direction - 95)
      .dot(dotSizeByFrame(3));
    Pen p21 = new Pen(p20).putDown()
      .pdMove(70, direction + 120)
      .dot(dotSizeByFrame(2));
    Pen p22 = new Pen(p20).putDown()
      .pdMove(90, direction + 160)
      .dot(dotSizeByFrame(2));
    Pen p23 = new Pen(p20).putDown()
      .pdMove(70, direction - 60)
      .dot(dotSizeByFrame(2));
    Pen p24 = new Pen(p20).putDown()
      .pdMove(90, direction - 150)
      .dot(dotSizeByFrame(2));
    new Pen(p21).putDown().moveTo(p22);
    new Pen(p23).putDown().moveTo(p24);
  }
}

void setup() {
  size(700, 700);
  frameRate(framerate);
  blendMode(SUBTRACT);
  background(color(0, 0, 0));
  stroke(color(120, 120, 120));
  strokeWeight(0.5);
  smooth();
  //noSmooth();
  frameRate(30);
  c = new Compound[num];
  for (int i = 0; i < num; i++)
    c[i] = new Compound(width / 2, height / 2, 360 / num * i);
  //new Pen().putDown().move(50, 50).dot(5).pickUp().pMove(50, PI/2).putDown().pdMove(300, -40);
}

void draw() {
  background(color(0, 0, 0));
  for (Compound i: c) {
    i.direction += cycleAngle;
    i.draw();
  }
  new Pen(width / 2, height / 2).putDown()
      .dot(15);    // draw center ring
}